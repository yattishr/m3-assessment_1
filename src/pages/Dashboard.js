import { StyleSheet, Text, View, TextInput, Button, Row, Alert, TouchableOpacity, Dimensions, Image } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  SafeAreaView,
  SafeAreaProvider,
  SafeAreaInsetsContext,
  useSafeAreaInsets,
  initialWindowMetrics,
} from 'react-native-safe-area-context';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Dashboard = ({ navigation }) => {
  return (

    <SafeAreaView>
      <View style={styles.container}>
        <Image
          style={styles.tinyLogo}
          source={{uri: 'https://reactnative.dev/img/tiny_logo.png'}}
        />
      </View>
    </SafeAreaView>
  )
}

export default Dashboard

const styles = StyleSheet.create({
  container: {
    paddingTop: windowHeight / 4,
    // backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    width: 120,
    height: 160,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
});