import { StyleSheet, Text, View, TextInput, Button, Row, Alert, TouchableOpacity, Dimensions, Image } from 'react-native'
import { React } from 'react'
import {
    SafeAreaView,
    SafeAreaProvider,
    SafeAreaInsetsContext,
    useSafeAreaInsets,
    initialWindowMetrics,
} from 'react-native-safe-area-context';
import { inline } from 'react-native-web/dist/cjs/exports/StyleSheet/compiler';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Separator = () => (
    <View style={styles.separator} />
);

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Login = ({ navigation }) => {

    return (
        <SafeAreaView>
            <View style={styles.container}>
                <Image source={{uri: './assets/login_background'}} />
            </View>
        
            <View style={styles.form}>
                <TextInput
                    style={styles.input}
                    placeholder="Username / Email"
                />
                <TextInput
                    style={styles.input}
                    textContentType='password'
                    placeholder="Password"
                />
            </View>

            <View style={styles.button}>
                <Button
                    title="Submit"
                    onPress={() => navigation.navigate('Dashboard')}
                />
                <Button
                    title="Cancel"
                    onPress={() => Alert.alert('Cancel button pressed')}
                />
            </View>

        </SafeAreaView>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ce93d8',
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderColor: '#ef5350',
        backgroundColor: '#fce4ec',
        color: '#fff',
        borderRadius: 5,
        borderBottomWidth: 1,
    },
    separator: {
        padding: 5,
        marginVertical: 8,
        marginHorizontal: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    button: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',        
    },
    form: {
        margin: 16,
        paddingTop: windowHeight/3,
    }
});